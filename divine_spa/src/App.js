import React, { useState } from "react";
import "./App.css";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import "react-bulma-components/dist/react-bulma-components.min.css";

// components
import NavBar from "./components/NavBar";
import Banner from "./components/Banner";
import Home from "./components/Home";
import About from "./components/About";
import Footer from "./components/Footer";
import SignUp from "./components/SignUp";
import Package from "./components/Package";
import Facility from "./components/Facility";
import Gallery from "./components/Gallery";
import Reservation from "./components/Reservation";
import HouseRules from "./components/HouseRules";
import ContactUs from "./components/ContactUs";
import AddPackage from "./components/AddPackage";
import Staff from "./components/Staff";
import UpdatePackage from "./components/UpdatePackage";
import UpdateStaff from "./components/UpdateStaff";
import LogIn from "./components/LogIn";
import ReservationList from "./components/ReservationList";
import Transaction from "./components/Transaction";

// create an instance to all our GraphQL components
const client = new ApolloClient({
  uri: "https://protected-shelf-72216.herokuapp.com/graphql"
});

function App() {
  const [username, setUsername] = useState(localStorage.getItem("username"));
  const [name, setName] = useState(localStorage.getItem("name"));
  const [userId, setUserId] = useState(localStorage.getItem("userId"));
  const [role, setRole] = useState(localStorage.getItem("role"));
  // console.log("this is the value of username: " + username);
  // console.log("this is the value of name: " + name);
  // console.log("this is the value of userId: " + userId);
  // console.log("this is the value of role: " + role);

  const updateSession = () => {
    setUsername(localStorage.getItem("username"));
    setName(localStorage.getItem("name"));
    setUserId(localStorage.getItem("userId"));
    setRole(localStorage.getItem("role"));
  };

  const Logout = () => {
    localStorage.clear();
    updateSession();
    return <Redirect to="/login" />;
  };

  const loggedUser = props => {
    return <LogIn {...props} updateSession={updateSession} />;
  };

  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <Banner />
        <NavBar username={username} userId={userId} role={role} />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/packages" component={Package} />
          <Route exact path="/facilities" component={Facility} />
          <Route exact path="/gallery" component={Gallery} />
          <Route exact path="/reservation" component={Reservation} />
          <Route exact path="/houseRules" component={HouseRules} />
          <Route exact path="/contact" component={ContactUs} />
          <Route exact path="/addPackage" component={AddPackage} />
          <Route exact path="/staff" component={Staff} />
          <Route exact path="/login" render={loggedUser} />
          <Route exact path="/reservationlist" component={ReservationList} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/transaction/:id" component={Transaction} />
          <Route exact path="/reservation" component={Reservation} />
        </Switch>
        <Footer />
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
