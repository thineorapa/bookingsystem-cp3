import React, { useState, useEffect } from "react";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import UpdatePackage from "./UpdatePackage";

// queries
import { getPackagesQuery } from "../queries/queries";

// mutation
import {
	createPackageMutation,
	deletePackageMutation
} from "../queries/mutations";

const AddPackage = props => {
	// console.log(props);
	const [name, setName] = useState("");
	const [price, setPrice] = useState("");

	useEffect(() => {
		// console.log("value of AddPackage: " + name);
		// console.log("value of Price: " + price);
		setName(name);
		setPrice(price);
	});

	const addPackageChangeHandler = e => {
		// console.log(e.target.value);
		setName(e.target.value);
	};

	const priceChangeHandler = e => {
		// console.log(e.target.value);
		setPrice(e.target.value);
	};

	const addPackage = e => {
		e.preventDefault();

		let newPackage = {
			name: name,
			price: JSON.parse(price)
		};

		// console.log(newPackage);
		props.createPackageMutation({
			variables: newPackage,

			refetchQueries: [
				{
					query: getPackagesQuery
				}
			]
		});

		// swal;
		Swal.fire({
			title: "Package Succesfully Added",
			icon: "success",
			confirmButtonText: "ok"
		});

		// clear inputbox
		// clear input boxes
		setName("");
		setPrice("");
	};

	const packageData = props.getPackagesQuery.getPackages
		? props.getPackagesQuery.getPackages
		: [];
	// console.log(packageData);

	// delete package
	const deletPackageHandler = e => {
		// console.log("you deleted package id: " + e.target.id);
		let id = e.target.id;

		Swal.fire({
			title: "Are you sure you want to delete it?",
			text: "It will permanently deleted!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deletePackageMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getPackagesQuery
						}
					]
				});
				Swal.fire("Deleted!", "Package has been deleted.", "success");
			}
		});
	};

	const [selectedId, setSelectedId] = useState("");
	const [close, setClose] = useState("");

	// const [packageDatas, setPackageDatas] = useState({
	// 	name: "",
	// 	price: ""
	// });

	const openHandler = e => {
		console.log(e.target.id);
		setSelectedId(e.target.id);
	};

	const closeHandler = e => {
		// console.log(e.target.id);
		setClose(e.target.id);
	};

	return (
		<div className="container-fluid pb-3 upper">
			<div className="text-center pt-5 head">Add Packages</div>
			<hr className="w-50" />
			<div className="reserved"></div>
			<div className="modal" id="updatePackage">
				<div className="modal-dialog-centered" role="document">
					<div className="modal-content">
						<div className="modal-header">
							<h5 className="modal-title">Update Package</h5>
							<button
								type="button"
								className="close"
								data-dismiss="modal"
								aria-label="Close"
							>
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<UpdatePackage
								selectedId={selectedId}
								close={setClose}
							/>
						</div>
					</div>
				</div>
			</div>

			<form onSubmit={addPackage}>
				<div className="form-row">
					<div className="form-group col-md-4 offset-2 mt-5">
						<label htmlFor="package">Package</label>
						<input
							type="text"
							className="form-control"
							id="package"
							placeholder="Package name"
							onChange={addPackageChangeHandler}
							value={name}
							required
						/>
					</div>

					<div className="form-group col-md-3 mt-5">
						<label htmlFor="package">Price</label>
						<input
							type="number"
							min="0"
							step="0.01"
							className="form-control"
							data-number-to-fixed="2"
							data-number-stepfactor="100"
							id="price"
							onChange={priceChangeHandler}
							value={price}
							required
						/>
					</div>
					<div className="col-md-2 mt-5 pt-2">
						<button
							type="submit"
							className="btn btn-secondary mt-4"
						>
							Add Package
						</button>
					</div>
				</div>

				<table className="table table-hover table-bordered table-success mt-5 w-50 mx-auto shadow">
					<thead>
						<tr className="table-active">
							<th scope="col">Package name</th>
							<th scope="col">Price</th>
							<th scope="col" className="pl-5">
								Action
							</th>
						</tr>
					</thead>
					<tbody>
						{packageData.map(service => {
							// console.log(service);
							return (
								<tr key={service.id} className="table-light">
									<td>{service.name}</td>
									<td>{service.price}</td>
									<td>
										<button
											type="button"
											className="btn btn-outline-warning"
											data-toggle="modal"
											data-target="#updatePackage"
											onClick={openHandler}
											id={service.id}
										>
											Update
										</button>

										<button
											type="button"
											id={service.id}
											className="btn btn-outline-danger ml-2"
											onClick={deletPackageHandler}
										>
											Delete
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</form>
		</div>
	);
};

export default compose(
	graphql(createPackageMutation, { name: "createPackageMutation" }),
	graphql(getPackagesQuery, { name: "getPackagesQuery" }),
	graphql(deletePackageMutation, { name: "deletePackageMutation" })
)(AddPackage);
