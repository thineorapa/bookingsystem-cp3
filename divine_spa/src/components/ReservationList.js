import React, { useState } from "react";
import { graphql } from "react-apollo";

// queries
import { getReservationsQuery } from "../queries/queries";

const ReservationList = props => {
	// console.log(props);
	const reservationsData = props.getReservationsQuery.getReservations
		? props.getReservationsQuery.getReservations
		: [];

	/*const [completed, setCompleted] = useState("completed");

	const transChangeHandler = e => {
		console.log("you click");
	};*/
	return (
		<div className="container-fluid pb-5 upper">
			<h1 className="text-center pt-5 head">Reservation List</h1>
			<hr className="w-50" />
			<div className="reserved"></div>
			<table className="table table-hover table-bordered table-success mt-5  mx-auto shadow">
				<thead>
					<tr className="table-active">
						<th scope="col">Customer Name</th>
						<th scope="col">Package type</th>
						<th scope="col">Staff</th>
						<th scope="col">Date Reserve</th>
						<th scope="col">Start Time</th>
						<th scope="col">End Time</th>
						<th scope="col">Quantity</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					{reservationsData.map(reservation => {
						// console.log(reservation);
						return (
							<tr key={reservation.id} className="table-light">
								<td>{reservation.name}</td>
								<td>{reservation.packages}</td>
								<td>{reservation.expertName}</td>
								<td>{reservation.reservationDate}</td>
								<td>{reservation.startTime}</td>
								<td>{reservation.endTime}</td>
								<td className="text-center">
									{reservation.quantity}
								</td>
								<td>
									<button
										type="button"
										className="btn btn-secondary btn-sm"
										data-toggle="tooltip"
										data-placement="top"
										title="Done"
										data-original-title="Tooltip on top"
										aria-describedby="tooltip408828"
									>
										Mark as completed
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>

			<div className="text-center mt-5 pt-5">
				<h1 className="head">Completed Transaction</h1>
				<hr className="w-50" />
			</div>
			<table className="table table-hover table-bordered table-success mt-5  mx-auto shadow">
				<thead>
					<tr className="table-active">
						<th scope="col">Customer Name</th>
						<th scope="col">Package type</th>
						<th scope="col">Staff</th>
						<th scope="col">Date Reserve</th>
						<th scope="col">Start Time</th>
						<th scope="col">End Time</th>
						<th scope="col">Quantity</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr className="table-light">
						<td>name</td>
						<td>packages</td>
						<td>expertName</td>
						<td>reservationDate</td>
						<td>startTime</td>
						<td>endTime</td>
						<td className="text-center">quantity</td>
						<td className="badge bg-primary badge-pill mt-2 text-dark">
							completed
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	);
};

export default graphql(getReservationsQuery, { name: "getReservationsQuery" })(
	ReservationList
);
