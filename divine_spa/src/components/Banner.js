import React from "react";

const Banner = () => {
	return (
		<div className="container-fluid text-center display-3 fixed-top banner">
			<img src="/images/logo.png" />
		</div>
	);
};

export default Banner;
