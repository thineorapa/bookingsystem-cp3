import React from "react";
import { Link } from "react-router-dom";
import "../App.css";

const Home = () => {
	return (
		<div className="upper">
			<section>
				<div
					id="carouselExampleControls"
					className="carousel slide"
					data-ride="carousel"
				>
					<div className="carousel-inner">
						<div className="carousel-item active">
							<img
								className="d-block w-100 carsImg"
								src="/images/img.jpg"
								alt="First slide"
							/>
						</div>
						<div className="carousel-item">
							<img
								className="d-block w-100 carsImg"
								src="/images/img2.jpg"
								alt="Second slide"
							/>
						</div>
						<div className="carousel-item">
							<img
								className="d-block w-100 carsImg"
								src="/images/img3.jpg"
								alt="Third slide"
							/>
						</div>
					</div>
					<a
						className="carousel-control-prev"
						href="#carouselExampleControls"
						role="button"
						data-slide="prev"
					>
						<span
							className="carousel-control-prev-icon"
							aria-hidden="true"
						></span>
						<span className="sr-only">Previous</span>
					</a>
					<a
						className="carousel-control-next"
						href="#carouselExampleControls"
						role="button"
						data-slide="next"
					>
						<span
							className="carousel-control-next-icon"
							aria-hidden="true"
						></span>
						<span className="sr-only">Next</span>
					</a>
				</div>
				<div className="con">
					<heading className="head">A WARM WELCOME</heading>
					<div className="reserved"></div>
					<p className="text-center par">
						We at Dream Spa provide various services to the nature
						of the clients. Wish how you would like to spend the
						time here we can talk and come to a conclusion.
					</p>
				</div>
			</section>

			<section className="row mt-5">
				<div className="card col-lg-3 col-sm-12 homeCard">
					<div className="spa">
						<h2>Ayurveda Spa</h2>
					</div>
					<img
						className="card-img-top"
						src="/images/ayurveda.jpg"
						alt="Card image cap"
					/>
					<div className="card-body">
						<p className="card-text">
							Some quick example text to build on the card title
							and make up the bulk of the card's content.
						</p>
					</div>
				</div>
				<div className="card col-lg-3 col-sm-12 homeCard">
					<div className="spa">
						<h2>Luxury Spa</h2>
					</div>
					<img
						className="card-img-top"
						src="/images/services2.jpg"
						alt="Card image cap"
					/>
					<div className="card-body">
						<p className="card-text">
							Some quick example text to build on the card title
							and make up the bulk of the card's content.
						</p>
					</div>
				</div>
				<div className="card col-lg-3 col-sm-12 homeCard">
					<div className="spa">
						<h2>Massage Spa</h2>
					</div>
					<img
						className="card-img-top"
						src="/images/services3.jpg"
						alt="Card image cap"
					/>
					<div className="card-body">
						<p className="card-text">
							Some quick example text to build on the card title
							and make up the bulk of the card's content.
						</p>
					</div>
				</div>
			</section>

			<section className="pack">
				<div className="col-md-3 offset-8 div1">
					<h2 className="treatment">
						Treatments Designed for You at Dream Spa
					</h2>
					<p className="dev text-justify">
						Developed by our team of wellness specialists and master
						aromatherapists, each treatment at Divine Spa consists
						of a relaxing ritual that combines the potent effects of
						oriental and western massage with the therapeutic
						benefits of custom-blended essential oils.
					</p>
				</div>
				<Link to="/packages">
					<a className="btn btn-secondary float-right mt-5 dev1button">
						View Menu
					</a>
				</Link>
			</section>

			<section className="pack1">
				<div className="col-md-3 offset-1 div2">
					<h2 className="treatment">The Art of Dream Spa</h2>
					<p className="dev text-justify">
						Prepare for an ethereal experience by soaking in our
						warm divine waters, a traditional Japanese practice held
						for centuries. Apart from physical and mental
						relaxation, onsen bathing is said to activate the body’s
						systems, stimulate circulation, and speed up metabolism.
					</p>
				</div>
				<Link to="/about">
					<a className="btn btn-secondary mt-5 dev2button">
						Learn More
					</a>
				</Link>
			</section>

			<section className="pack3">
				<div className="col-md-3 offset-8 div1">
					<h2 className="treatment">A Perfect Sanctuary</h2>
					<p className="dev text-justify">
						Dream Spa is conveniently located at the heart of Quezon
						City. Step out of the stream of your busy life and into
						our spa suites where our expert therapists will guide
						you through a personal journey of relaxation and
						rejuvenation.
					</p>
				</div>
				<Link to="/facilities">
					<a className="btn btn-secondary float-right mt-5 dev1button">
						Explore Our Suites
					</a>
				</Link>
			</section>

			<section className="pack4">
				<div className="col-md-3 offset-1 div2">
					<h2 className="treatment">A Perfect Sanctuary</h2>
					<p className="dev text-justify">
						Dream Spa is conveniently located at the heart of Quezon
						City. Step out of the stream of your busy life and into
						our spa suites where our expert therapists will guide
						you through a personal journey of relaxation and
						rejuvenation.
					</p>
				</div>
				<button
					type="button"
					className="btn btn-secondary mt-5 dev2button"
				>
					Explore Our Suites
				</button>
			</section>
		</div>
	);
};

export default Home;
