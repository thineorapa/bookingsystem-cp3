import React, { useState, useEffect } from "react";
import { createUserMutation } from "../queries/mutations";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";

const SignUp = props => {
	// hooks
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [contactNumber, setContactNumber] = useState("");
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [role, setRole] = useState("user");

	useEffect(() => {
		// console.log("value of name: " + name);
		// console.log("value of email: " + email);
		// console.log("value of contactNumber: " + contactNumber);
		// console.log("value of username: " + username);
		// console.log("value of password: " + password);
		// console.log("value of role: " + role);
		setName(name);
		setEmail(email);
		setContactNumber(contactNumber);
		setUsername(username);
		setPassword(password);
		setRole(role);
	});

	const nameChangeHandler = e => {
		// console.log(e.target.value);
		setName(e.target.value);
	};

	const roleChangeHandler = e => {
		// console.log(e.target.value);
		setRole(e.target.value);
	};

	const emailChangeHandler = e => {
		// console.log(e.target.value);
		setEmail(e.target.value);
	};

	const contactNumberChangeHandler = e => {
		// console.log(e.target.value);
		setContactNumber(e.target.value);
	};

	const usernameChangeHandler = e => {
		// console.log(e.target.value);
		setUsername(e.target.value);
	};

	const passwordChangeHandler = e => {
		// console.log(e.target.value);
		setPassword(e.target.value);
	};

	const addUser = e => {
		e.preventDefault();

		let newUser = {
			name: name,
			role: role,
			email: email,
			contactNumber: contactNumber,
			username: username,
			password: password
		};

		// console.log(newUser);
		props.createUserMutation({
			variables: newUser
		});

		// swal
		Swal.fire({
			title: "Sign Up Succesfully",
			icon: "success",
			confirmButtonText:
				'<a href="/login" class="has-text-light">Log In</a>'
		});

		// clear inputbox
		// clear input boxes
		setName("");
		setEmail("");
		setContactNumber("");
		setUsername("");
		setPassword("");
	};

	return (
		<div className="container-fluid pb-5 mt-5">
			<h1 className="text-center pt-5 head">SignUp now</h1>
			<hr className="w-50" />
			<div className="reserved"></div>
			<div className="row">
				<div className="col-md-5 offset-1">
					<img src="/images/sp.jpg" className="signupCover" />
				</div>
				<div className="col-md-6">
					<form onSubmit={addUser}>
						<div className="form">
							<div className="form-group w-50 mx-auto pt-1">
								<input
									type="hidden"
									id="role"
									onChange={roleChangeHandler}
									value={role}
								/>
							</div>
							<div className="form-group w-50 mx-auto pt-1">
								<label htmlFor="name">Full Name</label>
								<input
									type="text"
									className="form-control"
									id="name"
									placeholder="Full Name"
									onChange={nameChangeHandler}
									value={name}
									required
								/>
							</div>
							<div className="form-group w-50 mx-auto">
								<label htmlFor="contactNumer">
									Contact Number
								</label>
								<input
									type="number"
									className="form-control"
									id="contactNumer"
									placeholder="Contact Number"
									onChange={contactNumberChangeHandler}
									value={contactNumber}
									required
								/>
							</div>
							<div className="form-group w-50 mx-auto">
								<label htmlFor="inputEmail4">Email</label>
								<input
									type="email"
									className="form-control"
									id="inputEmail4"
									placeholder="Email"
									onChange={emailChangeHandler}
									value={email}
									required
								/>
							</div>
							<div className="form-group w-50 mx-auto">
								<label htmlFor="username">Username</label>
								<input
									type="text"
									className="form-control"
									id="username"
									placeholder="Username"
									onChange={usernameChangeHandler}
									value={username}
									required
								/>
							</div>
							<div className="form-group w-50 mx-auto">
								<label htmlFor="inputPassword4">Password</label>
								<input
									type="password"
									className="form-control"
									id="inputPassword4"
									placeholder="Password"
									onChange={passwordChangeHandler}
									value={password}
									required
								/>
							</div>
						</div>
						<button
							type="submit"
							className="btn btn-info btn-block mx-auto w-50"
						>
							Sign in
						</button>
					</form>
				</div>
			</div>
			<div>
				<h2 className="text-center mt-5">
					Already have an account? {""}
					<Link to="/login">
						<a>Click here to Login</a>
					</Link>
				</h2>
			</div>
		</div>
	);
};

export default graphql(createUserMutation, { name: "createUserMutation" })(
	SignUp
);
