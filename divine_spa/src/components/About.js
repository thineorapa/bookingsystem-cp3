import React from "react";
import { Link } from "react-router-dom";

const About = () => {
	return (
		<div className="container-fluid pt-5 upper">
			<h1 className="text-center head">About Us</h1>
			<hr className="w-50" />
			<p className="text-center">
				Little to know about us. We are here for you.
			</p>
			<div className="row aboutt hrline">
				<div className="col-md-7">
					<h1 className="m-5">Why Dream Spa?</h1>
					<p className="m-5 text-justify">
						On the other hand, we denounce with righteous
						indignation and dislike men who are so beguiled and
						demoralized by the charms of pleasure of the moment, so
						blinded by desire, that they cannot foresee the pain and
						trouble that are bound to ensue; and equal blame
						belongs.
					</p>
					<p className="m-5 text-justify">
						These cases are perfectly simple and easy to
						distinguish. In a free hour, when our power of choice is
						untrammelled and when nothing prevents our being able to
						do what we like best, every pleasure is to be welcomed
						and every pain in all together.
					</p>
				</div>
				<div className="col-md-4 mt-5 pt-5">
					<img src="/images/services2.jpg" />
				</div>
			</div>

			<div>
				<h1 className="text-center head mt-5">Our Services</h1>
				<hr className="w-50" />
				<p className="text-center">
					Here is a wide range of services we provide
				</p>
			</div>
			<div className="row mt-5">
				<div className="card col-lg-3 homeCard">
					<div className="spa">
						<h2>Ayurveda Spa</h2>
					</div>
					<img
						className="card-img-top"
						src="/images/ayurveda.jpg"
						alt="Card image cap"
					/>
					<div className="card-body">
						<p className="card-text">
							Some quick example text to build on the card title
							and make up the bulk of the card's content.
						</p>
					</div>
				</div>
				<div className="card col-lg-3 homeCard">
					<div className="spa">
						<h2>Luxury Spa</h2>
					</div>
					<img
						className="card-img-top"
						src="/images/services2.jpg"
						alt="Card image cap"
					/>
					<div className="card-body">
						<p className="card-text">
							Some quick example text to build on the card title
							and make up the bulk of the card's content.
						</p>
					</div>
				</div>
				<div className="card col-lg-3 homeCard">
					<div className="spa">
						<h2>Massage Spa</h2>
					</div>
					<img
						className="card-img-top"
						src="/images/services3.jpg"
						alt="Card image cap"
					/>
					<div className="card-body">
						<p className="card-text">
							Some quick example text to build on the card title
							and make up the bulk of the card's content.
						</p>
					</div>
				</div>
			</div>
			<div className="expert">
				<h1 className="mt-5 text-center head">Our Experts</h1>
				<hr className="w-50" />
				<p className="text-center mb-5">
					Know our experienced team of spa wellness. Customer
					satisfaction is their goal.
				</p>
			</div>

			<div className="row">
				<div className="col-md-2 mb-4">
					<div className="card h-100 expertCard">
						<h2 className="text-center mt-3 text-dark exp1">
							Diane Dizon
						</h2>
						<p className="text-center m-3 text-light">
							Herbal Spa Expert
						</p>
						<img
							src="/images/expert1.jpg"
							className="card-img-top exp"
							alt="..."
						/>
						<div className="card-body">
							<p className="card-text text-dark text-justify">
								This is a longer card with supporting text below
								as a natural lead-in to additional content. This
								content is a little bit longer.
							</p>
						</div>
					</div>
				</div>

				<div className="col-md-2 mb-4">
					<div className="card h-100 expertCard">
						<h2 className="text-center mt-3 text-dark exp1">
							Helen Marquez
						</h2>
						<p className="text-center m-3 text-light">
							Massage Head
						</p>
						<img
							src="/images/expert2.jpg"
							className="card-img-top exp"
							alt="..."
						/>
						<div className="card-body">
							<p className="card-text text-dark  text-justify">
								This is a longer card with supporting text below
								as a natural lead-in to additional content. This
								content is a little bit longer.
							</p>
						</div>
					</div>
				</div>

				<div className="col-md-2 mb-4">
					<div className="card h-100 expertCard">
						<h2 className="text-center mt-3 text-dark exp1">
							Jasmine Hanna
						</h2>
						<p className="text-center m-3 text-light">
							Aromatherapy Expert
						</p>
						<img
							src="/images/expert3.jpg"
							className="card-img-top exp"
							alt="..."
						/>
						<div className="card-body">
							<p className="card-text text-dark text-justify ">
								This is a longer card with supporting text below
								as a natural lead-in to additional content. This
								content is a little bit longer.
							</p>
						</div>
					</div>
				</div>

				<div className="col-md-2 mb-4">
					<div className="card h-100 expertCard">
						<h2 className="text-center mt-3 text-dark exp1">
							Suzzete Curtis
						</h2>
						<p className="text-center m-3 text-light">
							Herbal Spa Expert
						</p>
						<img
							src="/images/expert4.jpg"
							className="card-img-top exp"
							alt="..."
						/>
						<div className="card-body">
							<p className="card-text text-dark text-justify">
								This is a longer card with supporting text below
								as a natural lead-in to additional content. This
								content is a little bit longer.
							</p>
						</div>
					</div>
				</div>

				<div className="col-md-2 mb-4">
					<div className="card h-100 expertCard">
						<h2 className="text-center mt-3 text-dark exp1">
							Ivee Smith
						</h2>
						<p className="text-center text-light m-3">
							Aromatherapy Expert
						</p>
						<img
							src="/images/expert1.jpg"
							className="card-img-top exp"
							alt="..."
						/>
						<div className="card-body">
							<p className="card-text text-dark text-justify">
								This is a longer card with supporting text below
								as a natural lead-in to additional content. This
								content is a little bit longer.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default About;
