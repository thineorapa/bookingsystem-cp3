import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import { graphql } from "react-apollo";
import { getUsersQuery } from "../queries/mutations";
import Swal from "sweetalert2";

// mutation
import { logInMutation } from "../queries/mutations";

const LogIn = props => {
	// console.log(props);
	// hooks
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [loginSuccess, setLoginSuccess] = useState(false);

	useEffect(() => {
		// console.log("value of username: " + username);
		setUsername(username);
		// console.log("value of password: " + password);
		setPassword(password);
		// console.log(loginSuccess);
	});

	const usernameChangeHandler = e => {
		// console.log(e.target.value);
		setUsername(e.target.value);
	};

	const passwordChangeHandler = e => {
		// console.log("you click");
		setPassword(e.target.value);
	};

	const submitFormHandler = e => {
		e.preventDefault();
		console.log("submitting the log in form....");

		props
			.logInMutation({
				variables: {
					username: username,
					password: password
				}
			})
			.then(response => {
				// console.log(response);

				const data = response.data.logInUser;
				// console.log(data);

				if (data === null) {
					// swall tellin the user that his credentials are wrong
					Swal.fire({
						icon: "error",
						title: "Oops...",
						text: "Wrong Credentials!"
					});
				} else {
					// credentials are correct
					// syntax: setItem("key", "value")
					localStorage.setItem("username", data.username);
					localStorage.setItem("name", data.name);
					localStorage.setItem("userId", data.id);
					localStorage.setItem("role", data.role);
					props.updateSession();
					// console.log(props);
					setLoginSuccess(true);
				}
			});
	};

	if (!loginSuccess) {
		console.log("something went wrong....");
	} else {
		console.log("successful login");
		// return <Redirect to="/" />;
		window.location.href = "/";
	}

	return (
		<div className="container-fluid pb-5 mt-5">
			<div className="text-center pt-5">Welcome to Login Page</div>
			<hr className="w-50" />
			<div className="reserved"></div>
			<form onSubmit={submitFormHandler}>
				<div className="form-row">
					<div className="form-group col-md-4 offset-5 mx-auto mt-5">
						<label htmlFor="username">Username</label>
						<input
							type="text"
							className="form-control"
							id="username"
							placeholder="Username"
							onChange={usernameChangeHandler}
							value={username}
						/>

						<label htmlFor="password">Password</label>
						<input
							type="password"
							className="form-control"
							id="password"
							placeholder="Password"
							onChange={passwordChangeHandler}
							value={password}
						/>
					</div>
				</div>
				<button
					type="submit"
					className="btn btn-info btn-block mx-auto col-md-4 offset-5
			   "
				>
					Log in
				</button>
			</form>

			<div>
				<h2 className="text-center mt-5">
					Don't have an account yet? {""}
					<Link to="/signup">
						<a>Signup here</a>
					</Link>
				</h2>
			</div>
		</div>
	);
};

export default graphql(logInMutation, { name: "logInMutation" })(LogIn);
