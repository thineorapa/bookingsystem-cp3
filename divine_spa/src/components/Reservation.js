import React, { useState, useEffect } from "react";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Redirect } from "react-router-dom";

// queries
import {
	getReservationsQuery,
	getStaffsQuery,
	getPackagesQuery
} from "../queries/queries";

// mutation
import { createReservationMutation } from "../queries/mutations";

const Reservation = props => {
	// console.log(props);
	const packagesData = props.getPackagesQuery.getPackages
		? props.getPackagesQuery.getPackages
		: [];

	// hooks
	const [services, setServices] = useState("");
	const [staff, setStaff] = useState("");
	const [date, setDate] = useState("");
	const [startTime, setStartTime] = useState("");
	const [endTime, setEndTime] = useState("");
	const [quantity, setQuantity] = useState("");
	const [price, setPrice] = useState(0);
	const [transactionId, setTransactionId] = useState("");

	useEffect(() => {
		// console.log("value of services: " + services);
		// console.log("value of staff: " + staff);
		// console.log("value of date: " + date);
		// console.log("value of startTime: " + startTime);
		// console.log("value of endTime: " + endTime);
		// console.log("value of quantity: " + quantity);
		// console.log("value of price: " + price);
		// console.log("value of total: " + totalService);
		setServices(services);
		setStaff(staff);
		setDate(date);
		setStartTime(startTime);
		setEndTime(endTime);
		setQuantity(quantity);
		setPrice(price);
		setTransactionId(transactionId);
	});

	let totalService = price * quantity;
	// console.log(totalService);
	// console.log(packagesData);

	const serviceChangeHandler = e => {
		// console.log(e.target);
		setServices(e.target.value);

		if (props.getPackagesQuery.loading) {
			console.log("fetching...");
		} else {
			packagesData.map(singlePackage => {
				if (singlePackage.name === e.target.value) {
					setPrice(singlePackage.price);
				}
			});
		}
	};

	const staffChangeHandler = e => {
		// console.log(e.target.value);
		setStaff(e.target.value);
	};

	const dateChangeHandler = e => {
		// console.log(e.target.value);
		setDate(e.target.value);
	};

	const startTimeChangeHandler = e => {
		// console.log(e.target.value);
		setStartTime(e.target.value);
	};

	const endTimeChangeHandler = e => {
		// console.log(e.target.value);
		setEndTime(e.target.value);
	};

	const quantityChangeHandler = e => {
		// console.log(e.target.value);
		setQuantity(e.target.value * 1);
	};

	const totalChangeHandler = e => {
		// console.log(e.target.value);
		totalService();
	};

	const addReservation = e => {
		e.preventDefault();

		let newReservation = {
			packages: services,
			expertName: staff,
			reservationDate: date,
			startTime: startTime,
			endTime: endTime,
			quantity: quantity,
			price: price,
			total: totalService,
			userId: localStorage.userId,
			name: localStorage.name
		};

		// console.log(newReservation);

		props
			.createReservationMutation({
				variables: newReservation,

				refetchQueries: [
					{
						query: getReservationsQuery
					}
				]
			})
			.then(transaction => {
				// console.log(transaction.data.createReservation.userId);
				setTransactionId(transaction.data.createReservation.userId);

				window.location.href =
					"/transaction/" + transaction.data.createReservation.userId;
			});

		// insert SWAL here
		Swal.fire({
			position: "center",
			icon: "success",
			title: "Successfully Booked!",
			showConfirmButton: false,
			timer: 1500
		});

		// clear input boxes
		setServices("");
		setStaff("");
		setDate("");
		setStartTime("");
		setEndTime("");
		setQuantity("");
	};

	// services query
	const servicesOption = () => {
		let serviceData = props.getPackagesQuery;
		// console.log(serviceData);
		if (serviceData.loading) {
			return <option>loading services...</option>;
		} else {
			return serviceData.getPackages.map(service => {
				return (
					<option key={service.id} value={service.name}>
						{service.name}
					</option>
				);
			});
		}
	};

	// staff query
	const staffOptions = () => {
		let staffData = props.getStaffsQuery;
		// console.log(staffData);
		if (staffData.loading) {
			return <option>loading experts...</option>;
		} else {
			return staffData.getStaffs.map(staff => {
				return (
					<option key={staff.id} value={staff.name}>
						{staff.name}
					</option>
				);
			});
		}
	};

	return (
		<div className="container-fluid upper">
			<h1 className="text-center pt-5 head">Book Your Reservation Now</h1>
			<hr className="w-50" />
			<p className="text-center">
				Come and Experience the real essence of spa
			</p>
			<div className="reserved"></div>
			<h1 className="text-center m-5">Select Service & Staff</h1>

			<div className="pb-5">
				<form onSubmit={addReservation}>
					<div className="form-row">
						<div className="form-group col-md-5 offset-1">
							<label htmlFor="contactNumer">
								Available Services
							</label>
							<select
								onChange={serviceChangeHandler}
								className="browser-default custom-select"
							>
								<option disabled selected>
									Open this available services
								</option>
								{servicesOption()}
							</select>
						</div>

						<div className="form-group col-md-5 ">
							<label htmlFor="contactNumer">Spa Expert</label>
							<select
								onChange={staffChangeHandler}
								className="browser-default custom-select"
							>
								<option disabled selected>
									Choose your staff
								</option>
								{staffOptions()}
							</select>
						</div>
					</div>

					<h1 className="text-center m-5">Select Time & Quantity</h1>

					<div className="form-row">
						<div className="form-group col-md-4 offset-1">
							<label htmlFor="date">Im available on</label>
							<div>
								<input
									type="date"
									className="form-control"
									id="date"
									onChange={dateChangeHandler}
									value={date}
									required
								/>
							</div>
						</div>

						<div className="form-group col-md-2">
							<label htmlFor="date">Start Time</label>
							<div>
								<input
									type="time"
									className="form-control"
									id="date"
									onChange={startTimeChangeHandler}
									value={startTime}
									required
								/>
							</div>
						</div>

						<div className="form-group col-md-2">
							<label htmlFor="date">End Time</label>
							<div>
								<input
									type="time"
									className="form-control"
									id="date"
									onChange={endTimeChangeHandler}
									value={endTime}
									required
								/>
							</div>
						</div>

						<div className="form-group col-md-2">
							<label htmlFor="date">Quantity</label>
							<div>
								<input
									type="number"
									className="form-control"
									id="quantity"
									onChange={quantityChangeHandler}
									value={quantity}
									min={1}
									required
								/>
							</div>
						</div>

						<div className="form-group col-md-2">
							<div>
								<input
									type="hidden"
									className="form-control"
									id="total"
									onChange={totalChangeHandler}
									value={totalService}
								/>
							</div>
						</div>
					</div>
					{localStorage.getItem("userId") ? (
						<button
							type="submit"
							className="btn btn-info  col-md-2 reserveButton"
						>
							Reserve
						</button>
					) : (
						""
					)}
				</form>
			</div>
		</div>
	);
};

export default compose(
	graphql(getReservationsQuery, { name: "getReservationsQuery" }),
	graphql(getStaffsQuery, { name: "getStaffsQuery" }),
	graphql(getPackagesQuery, { name: "getPackagesQuery" }),
	graphql(createReservationMutation, { name: "createReservationMutation" })
)(Reservation);
