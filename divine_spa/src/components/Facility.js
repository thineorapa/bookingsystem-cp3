import React from "react";

const Facility = () => {
	return (
		<div className="container-fluid pb-5 upper">
			<h1 className="text-center pt-5 head">Facilities</h1>
			<hr className="w-50" />
			<p className="text-center">
				Our spa area invites you to relax and enjoy yourself after an
				active holiday! Take your time and soothe your body and mind
				with a short break!
			</p>
			<div className="reserved"></div>
			<div className="row">
				<div className="col-md-6">
					<div>
						<h2 className="text-center txt1">Wellness Suite</h2>
					</div>
					<img src="/images/faci3.jpg" className="image5 mt-3" />
				</div>

				<div className="col-md-6">
					<div>
						<h2 className="text-center txt1">Sweet Delight</h2>
					</div>
					<img src="/images/faci1.jpg" className="image5 mt-3" />
				</div>
			</div>

			<div className="row">
				<div className="col-md-6">
					<div>
						<h2 className="text-center txt2 mt-5">Sauna</h2>
					</div>
					<img src="/images/faci4.jpg" className="image6 mt-3" />
				</div>

				<div className="col-md-6">
					<div>
						<h2 className="text-center txt2 mt-5">Regular Room</h2>
					</div>
					<img src="/images/faci6.jpg" className="image6 mt-3" />
				</div>
			</div>
		</div>
	);
};

export default Facility;
