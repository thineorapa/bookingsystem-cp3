import React from "react";

const ContactUs = () => {
	return (
		<div className="container-fluid pt-5 pb-3 upper">
			<div className="row">
				<div className="col-md-6 text-center">
					<h1 className="head mb-2">Contact Us</h1>
					<p>
						Caswynn Building 134 Timog Ave Diliman, Quezon City 1103
						Metro Manila
					</p>
					<div className="mt-4">
						<p>Telephone no. +632 7755 7877</p>
						<p>Email: spareservations@imhotel.com</p>
					</div>
					<div className="mt-4">
						<p>
							Operation Hours <br />
							Mondayto Thursday <br />
							12:00 NN - 2:00 AM
						</p>
					</div>
					<div className="mt-4">
						<p>
							Friday to Sunday and Holidays <br />
							10:00 AM - 3:00 AM
						</p>
					</div>
				</div>
				<div className="col-md-6">
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.4100409860675!2d121.04148921525523!3d14.63265028021239!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b7afe6a39331%3A0xd367e6dc5e274032!2sCaswynn%20Building%2C%20134%20Timog%20Ave%2C%20Diliman%2C%20Quezon%20City%2C%201103%20Metro%20Manila!5e0!3m2!1sen!2sph!4v1575616448224!5m2!1sen!2sph"
						frameborder="0"
						allowfullscreen=""
						id="gmap"
					></iframe>
				</div>
			</div>
		</div>
	);
};

export default ContactUs;
