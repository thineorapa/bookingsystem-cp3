import React, { useState, useEffect } from "react";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
// queries
import { getPackagesQuery, getPackageQuery } from "../queries/queries";

// mutations
import { updatePackageMutation } from "../queries/mutations";

const UpdatePackage = props => {
	console.log(props);

	let service = props.getPackageQuery.getPackage
		? props.getPackageQuery.getPackage
		: {};

<<<<<<< HEAD
=======
	console.log(service.setName);
>>>>>>> 7dd7e3f114a1a619010f438b711b38e51ef0a835
	const [name, setName] = useState("");
	const [price, setPrice] = useState("");
	const [id, setId] = useState("");

	useEffect(() => {
		// console.log("name: " + name);
		// console.log("price: " + price);
		setName(name);
		setPrice(price);
	});

	if (!props.getPackageQuery.loading) {
		const setDefaultPackage = () => {
			setName(service.name);
			setPrice(service.price);
			setId(service.id);
		};

<<<<<<< HEAD
		if (id === "") {
=======
		if (service.id === "" && service.name && service.price) {
>>>>>>> 7dd7e3f114a1a619010f438b711b38e51ef0a835
			setDefaultPackage();
		}
	}

	const updateChangeHandler = e => {
		setName(e.target.value);
	};

	const updatePriceChangeHandler = e => {
		setPrice(e.target.value * 1);
	};

	const formSubmitHandler = e => {
		e.preventDefault();

		let updatePackage = {
			id: props.selectedId,
			name: name,
			price: price
		};

		props.UpdatePackageMutation({
			variables: updatePackage
		});

		Swal.fire({
			title: "Package Updated Succesfully",
			icon: "success",
			confirmButtonText:
				'<a href="/addPackage" class="has-text-light">OK</a>'
		});

		setName("");
		setPrice("");
		props.close();
	};

	return (
		<div className="container-fluid upper">
			<div className="text-center pt-5">Update Service Package</div>
			<hr className="w-50" />
			<div className="reserved"></div>
			<form onSubmit={formSubmitHandler}>
				<div className="form-row">
					<div className="form-group col-md-4 offset-1 mt-5">
						<label htmlFor="package">Package</label>
						<input
							type="text"
							className="form-control"
							id="package"
							placeholder="Package name"
							onChange={updateChangeHandler}
							value={name}
						/>
					</div>

					<div className="form-group col-md-4 mt-5">
						<label htmlFor="package">Price</label>
						<input
							type="number"
							min="0"
							step="0.01"
							className="form-control"
							data-number-to-fixed="2"
							data-number-stepfactor="100"
							id="price"
							onChange={updatePriceChangeHandler}
							value={price}
						/>
					</div>
					<div className="col-md-2 mt-5 pt-2">
						<button
							type="submit"
							className="btn btn-secondary btn-sm mt-2"
						>
							Save Changes
						</button>
					</div>
				</div>
			</form>
		</div>
	);
};

export default compose(
	graphql(updatePackageMutation, { name: "UpdatePackageMutation" }),
	graphql(getPackagesQuery, { name: "getPackagesQuery" }),
	graphql(getPackageQuery, {
		options: props => {
			// console.log(props.selectedId);

			return {
				variables: {
					id: props.selectedId
				}
			};
		},
		name: "getPackageQuery"
	})
)(UpdatePackage);
