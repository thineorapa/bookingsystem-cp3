import React from "react";

const Gallery = () => {
	return (
		<div className="container-fluid pb-5 upper">
			<h1 className="text-center pt-5 head">Gallery</h1>
			<hr className="w-50" />
			<p className="text-center">
				Take a look at our practice and process of defining spa!
			</p>
			<div className="reserved"></div>
			<div className="container row">
				<div className="col-md-4 con">
					<img src="/images/img2.jpg" className="image1" />
					<div className="overlay">
						<div className="text">Aromatherapy Spa</div>
					</div>
				</div>

				<div className="col-md-4 con">
					<img src="/images/img.jpg" className="image1" />
					<div className="overlay">
						<div className="text">Skin Care</div>
					</div>
				</div>

				<div className="col-md-4 con">
					<img src="/images/img4.jpg" className="image1" />
					<div className="overlay">
						<div className="text">Hot Stone</div>
					</div>
				</div>
			</div>

			<div className="container row">
				<div className="col-md-4 con">
					<img src="/images/procedures2.jpg" className="image1" />
					<div className="overlay">
						<div className="text">Skin Care</div>
					</div>
				</div>

				<div className="col-md-4 con">
					<img src="/images/hcover.jpeg" className="image1" />
					<div className="overlay">
						<div className="text">Sweet Delight</div>
					</div>
				</div>

				<div className="col-md-4 con">
					<img src="/images/procedures1.jpg" className="image1" />
					<div className="overlay">
						<div className="text">Herbal Spa</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Gallery;
