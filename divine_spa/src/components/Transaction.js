import React from "react";
import { graphql } from "react-apollo";

// queries
import { getUserQuery } from "../queries/queries";

const Transaction = props => {
	// console.log(props);

	let transaction = props.getUserQuery.getUser
		? props.getUserQuery.getUser
		: [];

	// console.log(transaction);
	const renderTable = () => {
		if (props.getUserQuery.loading) {
			return <p>fetching transaction...</p>;
		} else {
			return transaction.reservations.map(reservation => {
				return (
					<table className="table" key={reservation.id}>
						<tbody>
							<tr>
								<th scope="col">Reservation Date</th>
								<td></td>
								<td>{reservation.reservationDate}</td>
							</tr>
							<tr>
								<th scope="col">Package type</th>
								<td></td>
								<td>{reservation.packages}</td>
							</tr>
							<tr>
								<th scope="col">Staff</th>
								<td></td>
								<td>{reservation.expertName}</td>
							</tr>
							<tr>
								<th scope="col">Start Time</th>
								<td></td>
								<td>{reservation.startTime}</td>
							</tr>
							<tr>
								<th scope="col">End Time</th>
								<td></td>
								<td>{reservation.endTime}</td>
							</tr>
							<tr>
								<th scope="col">Price</th>
								<td></td>
								<td>{reservation.price}</td>
							</tr>
							<tr>
								<th scope="col">Quantity</th>
								<td></td>
								<td>{reservation.quantity}</td>
							</tr>
							<tr>
								<th scope="col">Total</th>
								<td></td>
								<td>{reservation.total}</td>
							</tr>
						</tbody>
					</table>
				);
			});
		}
	};

	return (
		<div className="container-fluid row pt-5 pb-5 upper">
			<div className="card border-secondary col-md-5 mx-auto">
				<div className="text-center pt-5">Reservation Details</div>
				<hr className="w-50" />
				<div className="reserved"></div>
				<div className="card-body">{renderTable()}</div>
			</div>
		</div>
	);
};

export default graphql(getUserQuery, {
	options: props => {
		// console.log(props.match.params);
		return {
			variables: {
				id: props.match.params.id
			}
		};
	},
	name: "getUserQuery"
})(Transaction);
