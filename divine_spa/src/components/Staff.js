import React, { useState, useEffect } from "react";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import UpdateStaff from "./UpdateStaff";

// queries
import { getStaffsQuery, getStaffQuery } from "../queries/queries";

// mutation
import { createStaffMutation, deleteStaffMutation } from "../queries/mutations";

const Staff = props => {
	// console.log(props);
	const [name, setName] = useState("");
	const [expertise, setExpertise] = useState("");
	const [status, setStatus] = useState(true);

	useEffect(() => {
		// console.log("value of staff: " + name);
		// console.log("value of expertise: " + expertise);
		// console.log("value of status: " + status);
		setName(name);
		setExpertise(expertise);
		setStatus(status);
	});

	const addStaffChangeHandler = e => {
		// console.log(e.target.value);
		setName(e.target.value);
	};

	const expertiseChangeHandler = e => {
		// console.log(e.target.value);
		setExpertise(e.target.value);
	};

	const addStaff = e => {
		e.preventDefault();

		let newStaff = {
			name: name,
			expertise: expertise,
			status: status
		};

		// console.log(newStaff);
		props.createStaffMutation({
			variables: newStaff,

			refetchQueries: [
				{
					query: getStaffsQuery
				}
			]
		});

		// swal;
		Swal.fire({
			title: "Staff Succesfully Added",
			icon: "success",
			confirmButtonText: "ok"
		});

		// clear inputbox
		// clear input boxes
		setName("");
		setExpertise("");
	};

	const staffData = props.getStaffsQuery.getStaffs
		? props.getStaffsQuery.getStaffs
		: [];
	// console.log(staffData);

	// delete staff
	const deleteStaffHandler = e => {
		// console.log("you deleted staff id: " + e.target.id);
		let id = e.target.id;

		Swal.fire({
			title: "Are you sure you want to delete it?",
			text: "It will permanently deleted!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteStaffMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getStaffsQuery
						}
					]
				});
				Swal.fire("Deleted!", "Staff has been deleted.", "success");
			}
		});
	};

	const [selectedId, setSelectedId] = useState("");
	const [close, setClose] = useState("");

	const openHandler = e => {
		// console.log(e.target.id);
		setSelectedId(e.target.id);
	};

	const closeHandler = e => {
		// console.log(e.target.id);
		setClose(e.target.id);
		window.location.reload(false);
	};

	return (
		<div className="container-fluid pb-3 upper">
			<h1 className="text-center pt-5 head">Add Your Staff</h1>
			<hr className="w-50" />
			<div className="reserved"></div>

			<div className="modal" id="updateStaff">
				<div className="modal-dialog-centered" role="document">
					<div className="modal-content">
						<div className="modal-header">
							<h5 className="modal-title">Update Staff</h5>
							<button
								type="button"
								className="close"
								data-dismiss="modal"
								aria-label="Close"
							>
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<UpdateStaff
								selectedId={selectedId}
								close={setClose}
							/>
						</div>
					</div>
				</div>
			</div>

			<form onSubmit={addStaff}>
				<div className="form-row">
					<div className="form-group col-md-3 offset-2 mt-5">
						<label htmlFor="package">Staff</label>
						<input
							type="text"
							className="form-control"
							id="staff"
							placeholder="Staff name"
							onChange={addStaffChangeHandler}
							value={name}
						/>
					</div>

					<div className="form-group col-md-3 mt-5">
						<label htmlFor="package">Expertise</label>
						<input
							type="text"
							className="form-control"
							id="expertise"
							placeholder="expertise"
							onChange={expertiseChangeHandler}
							value={expertise}
						/>
					</div>
					<div className="col-md-2 mt-5 pt-2">
						<button
							type="submit"
							className="btn btn-secondary  btn-block mt-4"
						>
							Add Staff
						</button>
					</div>
				</div>

				<table class="table table-bordered table-success mt-5 col-md-8 mx-auto shadow">
					<thead>
						<tr className="table-active">
							<th scope="col">Staff</th>
							<th scope="col">Expertise</th>
							<th scope="col"></th>
							<th scope="col">Status</th>
							<th scope="col" className="pl-5">
								Action
							</th>
						</tr>
					</thead>
					<tbody>
						{staffData.map(staff => {
							// console.log(staff.status);
							return (
								<tr key={staff.id}>
									<td>{staff.name}</td>
									<td>{staff.expertise}</td>
									<td></td>
									<td className="badge badge-primary badge-pill mt-3">
										{staff.status
											? "available"
											: "not available"}
									</td>
									<td>
										<button
											type="button"
											className="btn btn-outline-warning"
											data-toggle="modal"
											data-target="#updateStaff"
											id={staff.id}
											onClick={openHandler}
										>
											Update
										</button>
										<button
											type="button"
											id={staff.id}
											className="btn btn-outline-danger ml-2"
											onClick={deleteStaffHandler}
										>
											Delete
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</form>
		</div>
	);
};

export default compose(
	graphql(createStaffMutation, { name: "createStaffMutation" }),
	graphql(getStaffsQuery, { name: "getStaffsQuery" }),
	graphql(deleteStaffMutation, { name: "deleteStaffMutation" })
)(Staff);
