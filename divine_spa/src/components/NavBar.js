import React, { useState } from "react";
import { Navbar } from "react-bulma-components";
import { Link } from "react-router-dom";

const NavBar = props => {
	// console.log(props);
	const [open, setOpen] = useState(false);
	let customLink = "";

	if (!props.username) {
		customLink = (
			<Link to="/login" className="navbar-item">
				Login
			</Link>
		);
	} else {
		customLink = (
			<Link to="/logout" className="navbar-item">
				Logout
			</Link>
		);
	}

	if (props.role === "admin") {
		return (
			<nav
				className="navbar navbar-expand-lg navbar-dark bg-primary"
				id="navBar"
			>
				<button
					className="navbar-toggler"
					type="button"
					data-toggle="collapse"
					data-target="#navbarColor01"
					aria-controls="navbarColor01"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span className="navbar-toggler-icon"></span>
				</button>

				<div className="collapse navbar-collapse" id="navbarColor01">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item active">
							<Link to="/">
								<a className="nav-link">
									Home{" "}
									<span className="sr-only">(current)</span>
								</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/about">
								<a className="nav-link">About Us</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/Packages">
								<a className="nav-link">
									Procedures & Packages
								</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/facilities">
								<a className="nav-link">Facilities</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/gallery">
								<a className="nav-link">Gallery</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/reservation">
								<a className="nav-link">Reservation</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/houseRules">
								<a className="nav-link">House Rules</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/contact">
								<a className="nav-link">Contact Us</a>
							</Link>
						</li>

						<li className="nav-item dropdown">
							<a
								className="nav-link dropdown-toggle"
								id="navbarDropdown"
								role="button"
								data-toggle="dropdown"
								aria-haspopup="true"
								aria-expanded="false"
							>
								Manage Stuff
							</a>
							<div
								className="dropdown-menu"
								aria-labelledby="navbarDropdown"
							>
								<li className="nav-item dropdown-item">
									<Link to="/addPackage">
										<a className="nav-link text-dark">
											Add Package
										</a>
									</Link>
								</li>
								<li className="nav-item dropdown-item">
									<Link to="/staff">
										<a className="nav-link text-dark">
											Staff
										</a>
									</Link>
								</li>
								<li className="nav-item dropdown-item">
									<Link to="/reservationlist">
										<a className="nav-link text-dark">
											Reservation List
										</a>
									</Link>
								</li>
								{customLink}
							</div>
						</li>
					</ul>
				</div>
			</nav>
		);
	} else {
		return (
			<nav className="navbar navbar-expand-lg navbar-dark bg-primary" id="navBar1">
				<button
					className="navbar-toggler"
					type="button"
					data-toggle="collapse"
					data-target="#navbarColor01"
					aria-controls="navbarColor01"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span className="navbar-toggler-icon"></span>
				</button>

				<div className="collapse navbar-collapse" id="navbarColor01">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item active">
							<Link to="/">
								<a className="nav-link">
									Home{" "}
									<span className="sr-only">(current)</span>
								</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/about">
								<a className="nav-link">About Us</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/Packages">
								<a className="nav-link">
									Procedures & Packages
								</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/facilities">
								<a className="nav-link">Facilities</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/gallery">
								<a className="nav-link">Gallery</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/reservation">
								<a className="nav-link">Reservation</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/houseRules">
								<a className="nav-link">House Rules</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/contact">
								<a className="nav-link">Contact Us</a>
							</Link>
						</li>

						<li className="nav-item dropdown">
							<a
								className="navbar-item dropdown-toggle"
								id="navbarDropdown"
								role="button"
								data-toggle="dropdown"
								aria-haspopup="true"
								aria-expanded="false"
							>
								{props.username}
							</a>
							<div
								className="dropdown-menu"
								aria-labelledby="navbarDropdown"
							>
								{customLink}

								<li className="nav-item dropdown-item">
									<Link to={"/transaction/" + props.userId}>
										<a className="nav-link text-dark">
											My Reservation
										</a>
									</Link>
								</li>
							</div>
						</li>
					</ul>
				</div>
			</nav>
		);
	}

	return <div></div>;
};

export default NavBar;
