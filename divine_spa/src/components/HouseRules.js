import React from "react";

const HouseRules = () => {
	return (
		<div className="container-fluid pb-5 upper">
			<div className="container">
				<h1 className="text-center pt-5 head">House Rules</h1>
				<hr className="w-50" />
				<div className="reserved"></div>
				<div>
					<h1 className="text-center">Hours Of Operations</h1>
					<h1 className="text-center m-3">
						Monday – Thursday : 12:00 NN – 2:00 AM. Last booking for
						spa treatments is only up until 12:30 AM
					</h1>
					<h1 className="text-center mt-3">
						Friday – Sunday and Holidays: 10:00 AM – 3:00 AM. Last
						booking for spa treatments is only up until 1:30 AM
					</h1>
				</div>

				<div>
					<h1>Facilities</h1>
					<p>
						Steam, Sauna and Showers Onsen Changing areas Lockers
						Only hotel guests, spa members and paying guests may use
						the facilities. Fitness Center and Swimming pool are for
						Hotel Guests only.
					</p>

					<h1 className="mt-3 ">Reservations</h1>
					<p className="mt-2">
						To assist you further with your needs or requirements,
						we recommend that you book all spa treatments and
						services in advance. In addition, we advise our guests
						to settle the prepayment to guarantee their booking.
					</p>

					<h1 className="mt-3 ">Amenities</h1>
					<p className="mt-2">
						Male and female changing areas have individual lockers
						and shower facilities.
					</p>

					<h1 className="mt-3 ">Valuables</h1>
					<p className="mt-2">
						We strongly encourage our guests to keep all valuable
						items safe throughout their spa journey. The spa does
						not take responsibility for any lost or stolen items or
						damage to personal property.
					</p>

					<h1 className="mt-3 ">Appointment Time</h1>
					<p className="mt-2">
						Please arrive at least One (1) HOUR prior to your
						appointment time. This gives our staffs enough time to
						prepare thus allowing you to enjoy our Onsen facilities.
						Arriving late will lessen your treatment time reducing
						its effectiveness and your satisfaction. Also, late
						arrivals will still be charged in full.
					</p>

					<h1 className="mt-3 ">Cancellation Policy</h1>
					<p className="mt-2">
						Cancellation of spa treatment should be made at least 2
						hours prior to appointment time, otherwise we will be
						charging a 50% cancellation fee to guest. Consequently,
						Onsen Spa will be charging full treatment cost for “No
						show” guests.
					</p>

					<h1 className="mt-3 ">Special Consideration</h1>
					<p className="mt-2">
						Your health and safety come first. Guests should inform
						spa therapist of any medical conditions before the start
						of the spa treatment. Similarly, let your therapist know
						of any skin allergies, if pregnant, and other medical
						conditions. Also, indicate any areas of your body that
						may require special attention. During your treatment,
						advise your therapist if you are suffering from any pain
						or discomfort.
					</p>

					<h1 className="mt-3 ">Smoking and Alcohol</h1>
					<p className="mt-2">
						Smoking and the consumption of alcohol within the spa
						are strictly prohibited. Consuming alcohol and
						caffeinated drinks before or immediately after spa
						treatment is by all means not recommended.
					</p>

					<h1 className="mt-3 ">Payment</h1>
					<p className="mt-2">
						Divine Spa accepts cash and all major credit cards.
						Hotel guests may charge spa treatments to their room.
						Spa treatment packages are in Philippine Peso and are
						inclusive of 12% VAT and 0.75% Local Tax.
					</p>

					<h1 className="mt-3 ">Minimum Age</h1>
					<p className="mt-2">
						All children below 16 years of age must be accompanied
						by an adult while inside the spa at all times.
					</p>

					<h1 className="mt-3 ">Etiquette</h1>
					<p className="mt-2">
						Divine Spa is a refuge for serenity and calmness.We seek
						to maintain a relaxing atmosphere for all our guests and
						members. In this regard, we advise our guests to turn
						off all electronic gadgets whilst inside our premises. A
						detailed Onsen Spa rules and regulations manual is
						available in our public places for guests reference. I’M
						Onsen Spa reserves the right to refuse the admission
						and/or service of guests who violate our house rules.
					</p>

					<h1 className="mt-3 ">Photos and Videos</h1>
					<p className="mt-2">
						As a courtesy, please avoid taking photos that include
						other guests without their permission. Consequently,
						camera use in all changing rooms is strictly prohibited.
					</p>

					<h1 className="mt-3 ">Prohibited Items</h1>
					<p className="mt-2">
						The following items are not permitted at any time:
					</p>
					<p className="mt-2">
						Firearms and explosives Chemical weapons Knives Sharp or
						spiked items Animals and pets
					</p>
				</div>
			</div>
		</div>
	);
};

export default HouseRules;
