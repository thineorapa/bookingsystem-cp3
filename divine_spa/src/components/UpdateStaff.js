import React, { useState, useEffect } from "react";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";

// queries
import { getStaffQuery } from "../queries/queries";

// mutations
import { updateStaffMutation } from "../queries/mutations";

const UpdateStaff = props => {
	// console.log(props);

	let staff = props.getStaffQuery.getStaff
		? props.getStaffQuery.getStaff
		: {};

	console.log(staff);

	const [name, setName] = useState("");
	const [expertise, setExpertise] = useState("");
	const [status, setStatus] = useState(true);
	const [staffId, setStaffId] = useState("");

	useEffect(() => {
		// console.log("name: " + name);
		// console.log("expertise: " + expertise);
		// console.log("staffId: " + staffId);
		// console.log("selectedId: " + props.selectedId);
		setName(name);
		setExpertise(expertise);
		setStaffId(staffId);
		setStatus(status);
	});

	if (!props.getStaffQuery.loading) {
		const setDefaultStaff = () => {
			setStaffId(staff.id);
			setName(staff.name);
			setExpertise(staff.expertise);
		};
		if (staffId === "" && staff.name && staff.name) {
			setDefaultStaff();
		}
	}

	const updateNameChangeHandler = e => {
		setName(e.target.value);
	};

	const updateExpertiseChangeHandler = e => {
		setExpertise(e.target.value);
	};

	const updateStaffHandler = e => {
		e.preventDefault();
		let updateStaff = {
			id: staff.id,
			name: name,
			expertise: expertise,
			status: status
		};
		props.updateStaffMutation({
			variables: updateStaff
		});
		Swal.fire({
			title: "Staff Updated Succesfully",
			icon: "success",
			confirmButtonText: '<a href="/staff" class="has-text-light">OK</a>'
		});
		setName("");
		setExpertise("");
		props.close();
	};

	return (
		<div className="container-fluid pb-3 upper">
			<div className="text-center pt-5">Update Your Staff</div>
			<hr className="w-50" />
			<div className="reserved"></div>
			<form onSubmit={updateStaffHandler}>
				<div className="form-row" key={props.staffId}>
					<div className="form-group col-md-3 offset-2 mt-5">
						<label htmlFor="package">Staff</label>
						<input
							type="text"
							className="form-control"
							id="staff"
							placeholder="Staff name"
							onChange={updateNameChangeHandler}
							value={name}
						/>
					</div>

					<div className="form-group col-md-3 mt-5">
						<label htmlFor="package">Expertise</label>
						<input
							type="text"
							className="form-control"
							id="expertise"
							placeholder="expertise"
							onChange={updateExpertiseChangeHandler}
							value={expertise}
						/>
					</div>
					<div className="col-md-2 mt-5 pt-2">
						<button
							type="submit"
							className="btn btn-secondary btn-sm mt-4"
						>
							Update
						</button>
					</div>
				</div>
			</form>
		</div>
	);
};

export default compose(
	graphql(updateStaffMutation, { name: "updateStaffMutation" }),
	graphql(getStaffQuery, {
		options: props => {
			return {
				variables: {
					id: props.selectedId
				}
			};
		},
		name: "getStaffQuery"
	})
)(UpdateStaff);
