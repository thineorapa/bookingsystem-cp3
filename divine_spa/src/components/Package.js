import React from "react";
import { Link } from "react-router-dom";

const Package = () => {
	return (
		<div className="container-fluid upper">
			<h1 className="text-center pt-5 head">Packages & Procedure</h1>
			<hr className="w-50" />
			<p className="text-center mb-5">
				Little to know about us. We are here for you.
			</p>
			<div>
				<img src="/images/package.jpg" />
			</div>
			<div className="row treat">
				<div className="col-md-3 offset-1 bg-primary procedure">
					<h1 className="text-center aroma">Aromatheraphy</h1>
					<h2 className="text-center mt-3">Duration: 1-2 hrs</h2>
					<h2 className="text-center mt-3">₱ 3000</h2>
					<img src="/images/procedures1.jpg" className="prod" />
					<p className="text-justify m-3">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Morbi hendrerit elit turpis, a porttitor tellus
						sollicitudin at. Class aptent taciti sociosqu ad litora
						torquent per conubia nostra, per inceptos himenaeos.
					</p>
				</div>
				<div className="col-md-8">
					<h1 className="h1Spa text-center mb-3">Spa Treatment</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Morbi hendrerit elit turpis, a porttitor tellus
						sollicitudin at. Class aptent taciti sociosqu ad litora
						torquent per conubia nostra, per inceptos
						himenaeos.Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Morbi hendrerit elit turpis, a
						porttitor tellus sollicitudin at. Class aptent taciti
						sociosqu ad litora torquent per conubia nostra, per
						inceptos himenaeos.
					</p>
					<h2 className="mt-5 mb-3">Effects</h2>
					<p className="mb-2">
						<i class="fas fa-check"></i> Makes the body slenderer
						and the figure more beautiful
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Effectively reduces
						cellulite problems
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Cleanses the body of waste
						substances and repairs blood and lymphatic circulation
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Makes the skin smoother,
						more elastic, softer and revitalises dry skin
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Reduces muscular and joint
						pains
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Relaxes, refreshes and
						gives a wonderful feeling, reduces fatigue and stress
					</p>
				</div>
			</div>
			{/* end of package1*/}

			<div className="row treat">
				<div className="col-md-7 offset-1">
					<h1 className="h1Spa text-center mb-3">Spa Treatment</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Morbi hendrerit elit turpis, a porttitor tellus
						sollicitudin at. Class aptent taciti sociosqu ad litora
						torquent per conubia nostra, per inceptos
						himenaeos.Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Morbi hendrerit elit turpis, a
						porttitor tellus sollicitudin at. Class aptent taciti
						sociosqu ad litora torquent per conubia nostra, per
						inceptos himenaeos.
					</p>
					<h2 className="mt-5 mb-3">Effects</h2>
					<p className="mb-2">
						<i class="fas fa-check"></i> Makes the body slenderer
						and the figure more beautiful
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Effectively reduces
						cellulite problems
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Cleanses the body of waste
						substances and repairs blood and lymphatic circulation
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Makes the skin smoother,
						more elastic, softer and revitalises dry skin
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Reduces muscular and joint
						pains
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Relaxes, refreshes and
						gives a wonderful feeling, reduces fatigue and stress
					</p>
				</div>
				<div className="col-md-3 bg-primary procedure1">
					<h1 className="text-center aroma">Skin Care</h1>
					<h2 className="text-center mt-3">Duration: 1-2 hrs</h2>
					<h2 className="text-center mt-3">₱ 3500</h2>
					<img src="/images/procedures2.jpg" className="prod" />
					<p className="text-justify m-3">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Morbi hendrerit elit turpis, a porttitor tellus
						sollicitudin at. Class aptent taciti sociosqu ad litora
						torquent per conubia nostra, per inceptos himenaeos.
					</p>
				</div>
			</div>
			{/*end of packages2*/}
			<div className="row treat">
				<div className="col-md-3 offset-1 bg-primary procedure">
					<h1 className="text-center aroma">Herbal Spa</h1>
					<h2 className="text-center mt-3">Duration: 1-2 hrs</h2>
					<h2 className="text-center mt-3">₱ 3500</h2>
					<img src="/images/procedures3.jpg" className="prod" />
					<p className="text-justify m-3">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Morbi hendrerit elit turpis, a porttitor tellus
						sollicitudin at. Class aptent taciti sociosqu ad litora
						torquent per conubia nostra, per inceptos himenaeos.
					</p>
				</div>
				<div className="col-md-8">
					<h1 className="h1Spa text-center mb-3">Spa Treatment</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Morbi hendrerit elit turpis, a porttitor tellus
						sollicitudin at. Class aptent taciti sociosqu ad litora
						torquent per conubia nostra, per inceptos
						himenaeos.Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Morbi hendrerit elit turpis, a
						porttitor tellus sollicitudin at. Class aptent taciti
						sociosqu ad litora torquent per conubia nostra, per
						inceptos himenaeos.
					</p>
					<h2 className="mt-5 mb-3">Effects</h2>
					<p className="mb-2">
						<i class="fas fa-check"></i> Makes the body slenderer
						and the figure more beautiful
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Effectively reduces
						cellulite problems
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Cleanses the body of waste
						substances and repairs blood and lymphatic circulation
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Makes the skin smoother,
						more elastic, softer and revitalises dry skin
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Reduces muscular and joint
						pains
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Relaxes, refreshes and
						gives a wonderful feeling, reduces fatigue and stress
					</p>
				</div>
			</div>
			{/*end of package3*/}
			<div className="row treat">
				<div className="col-md-7 offset-1">
					<h1 className="h1Spa text-center mb-3">Spa Treatment</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Morbi hendrerit elit turpis, a porttitor tellus
						sollicitudin at. Class aptent taciti sociosqu ad litora
						torquent per conubia nostra, per inceptos
						himenaeos.Lorem ipsum dolor sit amet, consectetur
						adipiscing elit. Morbi hendrerit elit turpis, a
						porttitor tellus sollicitudin at. Class aptent taciti
						sociosqu ad litora torquent per conubia nostra, per
						inceptos himenaeos.
					</p>
					<h2 className="mt-5 mb-3">Effects</h2>
					<p className="mb-2">
						<i class="fas fa-check"></i> Makes the body slenderer
						and the figure more beautiful
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Effectively reduces
						cellulite problems
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Cleanses the body of waste
						substances and repairs blood and lymphatic circulation
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Makes the skin smoother,
						more elastic, softer and revitalises dry skin
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Reduces muscular and joint
						pains
					</p>
					<p className="mb-2">
						<i class="fas fa-check"></i> Relaxes, refreshes and
						gives a wonderful feeling, reduces fatigue and stress
					</p>
				</div>
				<div className="col-md-3 bg-primary procedure1">
					<h1 className="text-center aroma">Sweet Delight</h1>
					<h2 className="text-center mt-3">Duration: 1-2 hrs</h2>
					<h2 className="text-center mt-3">₱ 2500</h2>
					<img src="/images/img2.jpg" className="prod" />
					<p className="text-justify m-3">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Morbi hendrerit elit turpis, a porttitor tellus
						sollicitudin at. Class aptent taciti sociosqu ad litora
						torquent per conubia nostra, per inceptos himenaeos.
					</p>
				</div>
			</div>
			<div className="row purchase">
				<div className="col-md-2 offset-2">
					<img src="/images/pac.png" className="mt-5 pt-5 pic" />
				</div>
				<div className="col-md-6 mb-5">
					<div className="h1Spa">
						<Link to="/reservation">
							<a>Book now for a Spa</a>
						</Link>
					</div>
					<p>
						Hope you have seen our wide range of services for spa
						and herbal treatments. Book now for your convienent
						schedule and enjoy the spa treatment and welness of
						life.
					</p>
				</div>
			</div>
		</div>
	);
};

export default Package;
