import React from "react";
import { Link } from "react-router-dom";

const Footer = () => {
	return (
		<div className="container-fluids">
			<div className="bg-primary">
				<div className="container">
					<div className="row py-4 d-flex align-items-center">
						<div className="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
							<h6 className="mb-0">
								Get connected with us on social networks!
							</h6>
						</div>
						<div className="col-md-6 col-lg-7 text-center text-md-right">
							<a className="fb-ic">
								<i className="fab fa-facebook-f white-text mr-4">
									{" "}
								</i>
							</a>

							<a className="tw-ic">
								<i className="fab fa-twitter white-text mr-4">
									{" "}
								</i>
							</a>

							<a className="gplus-ic">
								<i className="fab fa-google-plus-g white-text mr-4">
									{" "}
								</i>
							</a>

							<a className="li-ic">
								<i className="fab fa-linkedin-in white-text mr-4">
									{" "}
								</i>
							</a>

							<a className="ins-ic">
								<i className="fab fa-instagram white-text"> </i>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div className="container text-center text-md-left mt-5">
				<div className="row mt-3">
					<div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
						<h6 className="text-uppercase font-weight-bold">
							Dream Spa
						</h6>
						<hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" />
						<p>
							We at Dream Spa provide various services to the
							nature of the clients. Wish how you would like to
							spend the time here we can talk and come to a
							conclusion.
						</p>
					</div>

					<div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
						<h6 className="text-uppercase font-weight-bold">
							Services
						</h6>
						<hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" />
						<p>
							<Link to="/packages">
								<a>Aromatherapy Spa</a>
							</Link>
						</p>
						<p>
							<Link to="/packages">
								<a>Herbal Spa</a>
							</Link>
						</p>
						<p>
							<Link to="/packages">
								<a>Skin Care</a>
							</Link>
						</p>
						<p>
							<Link to="/packages">
								<a>Sweet Delight</a>
							</Link>
						</p>
					</div>

					<div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
						<h6 className="text-uppercase font-weight-bold">
							Useful links
						</h6>
						<hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" />
						<p>
							<a href="#!">Your Account</a>
						</p>
						<p>
							<a href="#!">Become an Affiliate</a>
						</p>
						<p>
							<a href="#!">Shipping Rates</a>
						</p>
						<p>
							<a href="#!">Help</a>
						</p>
					</div>

					<div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
						<h6 className="text-uppercase font-weight-bold">
							Contact
						</h6>
						<hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" />
						<p>
							<i className="fas fa-home mr-3"></i> Caswynn
							Building, 3rd floor Quezon City
						</p>
						<p>
							<i className="fas fa-envelope mr-3"></i>{" "}
							dreamspa@gmail.com
						</p>
						<p>
							<i className="fas fa-phone mr-3"></i> + 01 234 567
							88
						</p>
						<p>
							<i className="fas fa-print mr-3"></i> + 01 234 567
							89
						</p>
					</div>
				</div>
			</div>

			<div className="text-center py-1">
				© 2019 Copyright:
				<a href="#"> Dream Spa</a>
			</div>
			<div className="text-center">
				<p>
					Disclaimer: No Copyright intended for educational purposes
					only
				</p>
			</div>
		</div>
	);
};

export default Footer;
