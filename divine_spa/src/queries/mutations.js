import { gql } from "apollo-boost";

const createUserMutation = gql`
    mutation(
        $name: String!
        $role: String!
        $email: String!
        $username: String!
        $password: String!
        $contactNumber: String!
    ) {
        createUser(
            name: $name
            role: $role
            email: $email
            username: $username
            password: $password
            contactNumber: $contactNumber
        ) {
            id
            name
            email
            role
            username
            password
            contactNumber
        }
    }
`;

const createPackageMutation = gql`
    mutation($name: String!, $price: Int!) {
        createPackage(name: $name, price: $price) {
            id
            name
            price
        }
    }
`;

const createStaffMutation = gql`
    mutation($name: String!, $expertise: String!, $status: Boolean!) {
        createStaff(name: $name, expertise: $expertise, status: $status) {
            id
            name
            expertise
            status
        }
    }
`;

const createReservationMutation = gql`
    mutation(
        $userId: String!
        $name: String!
        $price: Int!
        $reservationDate: String!
        $quantity: Int!
        $total: Int!
        $startTime: String!
        $endTime: String!
        $expertName: String!
        $packages: String!
    ) {
        createReservation(
            userId: $userId
            name: $name
            price: $price
            reservationDate: $reservationDate
            quantity: $quantity
            total: $total
            startTime: $startTime
            endTime: $endTime
            expertName: $expertName
            packages: $packages
        ) {
            id
            userId
            name
            reservationDate
            quantity
            startTime
            endTime
            expertName
            packages
            price
            total
        }
    }
`;

const logInMutation = gql`
    mutation($username: String!, $password: String!) {
        logInUser(username: $username, password: $password) {
            username
            password
            name
            id
            role
        }
    }
`;

const deletePackageMutation = gql`
    mutation($id: String!) {
        deletePackage(id: $id)
    }
`;

const deleteStaffMutation = gql`
    mutation($id: String!) {
        deleteStaff(id: $id)
    }
`;

const updatePackageMutation = gql`
    mutation($id: ID!, $name: String!, $price: Int!) {
        updatePackage(id: $id, name: $name, price: $price) {
            id
            name
            price
        }
    }
`;

const updateStaffMutation = gql`
    mutation($id: ID!, $name: String!, $expertise: String!, $status: Boolean) {
        updateStaff(
            id: $id
            name: $name
            expertise: $expertise
            status: $status
        ) {
            id
            name
            expertise
            status
        }
    }
`;

export {
    createUserMutation,
    logInMutation,
    createPackageMutation,
    createStaffMutation,
    createReservationMutation,
    deletePackageMutation,
    deleteStaffMutation,
    updatePackageMutation,
    updateStaffMutation
};
