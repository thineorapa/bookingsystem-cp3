import { gql } from "apollo-boost";

const getPackagesQuery = gql`
	{
		getPackages {
			id
			name
			price
		}
	}
`;

const getStaffsQuery = gql`
	{
		getStaffs {
			id
			name
			expertise
			status
		}
	}
`;

const getReservationsQuery = gql`
	{
		getReservations {
			id
			reservationDate
			quantity
			startTime
			endTime
			expertName
			packages
			userId
			name
			price
			total
		}
	}
`;

const getReservationQuery = gql`
	query($id: ID!) {
		getReservation(id: $id) {
			id
			reservationDate
			quantity
			startTime
			endTime
			expertName
			packages
			userId
			name
			price
			total
		}
	}
`;

const getStaffQuery = gql`
	query($id: ID!) {
		getStaff(id: $id) {
			id
			name
			expertise
			status
		}
	}
`;

const getPackageQuery = gql`
	query($id: ID!) {
		getPackage(id: $id) {
			id
			name
			price
		}
	}
`;

const getUserQuery = gql`
	query($id: ID!) {
		getUser(id: $id) {
			id
			name
			role
			reservations {
				id
				reservationDate
				name
				total
				price
				expertName
				startTime
				endTime
				quantity
				packages
			}
		}
	}
`;

export {
	getPackagesQuery,
	getStaffQuery,
	getStaffsQuery,
	getReservationsQuery,
	getReservationQuery,
	getUserQuery,
	getPackageQuery
};
