const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDateTime } = require("graphql-iso-date");
const bcrypt = require("bcrypt");

// mongoose model
const User = require("../models/User");
const Reservation = require("../models/Reservation");
const Package = require("../models/Package");
const Staff = require("../models/Staff");

const customScalarResolver = {
	Date: GraphQLDateTime
};

const typeDefs = gql`
	type UserType {
		id: ID!
		role: String!
		name: String!
		email: String!
		contactNumber: String!
		username: String!
		password: String!
		reservations: [ReservationType]
	}

	type ReservationType {
		id: ID!
		userId: ID!
		name: String!
		price: Int!
		reservationDate: String!
		startTime: String!
		endTime: String!
		quantity: Int
		total: Int!
		packages: String!
		expertName: String!
	}

	type PackageType {
		id: ID!
		price: Int!
		name: String!
	}

	type StaffType {
		id: ID!
		name: String!
		expertise: String!
		status: Boolean
	}

	type Query {
		getUsers: [UserType]
		getReservations: [ReservationType]
		getPackages: [PackageType]
		getStaffs: [StaffType]
		getStaff(id: ID!): StaffType
		getUser(id: ID!): UserType
		getReservation(id: ID!): ReservationType
		getPackage(id: ID!): PackageType
	}

	type Mutation {
		createUser(
			name: String!
			role: String!
			email: String!
			username: String!
			password: String!
			contactNumber: String!
		): UserType

		createReservation(
			userId: String!
			name: String!
			price: Int!
			reservationDate: String!
			quantity: Int!
			total: Int!
			startTime: String!
			endTime: String!
			packages: String!
			expertName: String!
		): ReservationType

		createStaff(
			name: String!
			expertise: String!
			status: Boolean
		): StaffType

		createPackage(price: Int!, name: String!): PackageType

		updateUser(
			id: ID!
			name: String!
			email: String!
			contactNumber: String!
		): UserType

		updateReservation(
			id: ID!
			userId: ID!
			name: String!
			price: Int!
			reservationDate: String!
			quantity: Int!
			total: Int!
			startTime: String!
			endTime: String!
			packages: String!
			expertName: String!
		): ReservationType

		updateStaff(
			id: ID!
			name: String!
			expertise: String!
			status: Boolean
		): StaffType

		updatePackage(id: ID!, name: String!, price: Int!): PackageType

		deleteUser(id: String): Boolean
		deleteReservation(id: String!): Boolean
		deletePackage(id: String): Boolean
		deleteStaff(id: String!): Boolean

		logInUser(username: String!, password: String!): UserType
	}
`;

const resolvers = {
	Query: {
		getUsers: () => {
			return User.find({});
		},

		getReservations: () => {
			return Reservation.find({});
		},

		getPackages: () => {
			return Package.find({});
		},

		getStaffs: () => {
			return Staff.find({});
		},

		getStaff: (_, args) => {
			return Staff.findById(args.id);
		},

		getUser: (_, args) => {
			return User.findById(args.id);
		},

		getReservation: (_, args) => {
			return Reservation.findById(args.id);
		},

		getPackage: (_, args) => {
			// console.log("nag Package query ka")
			// console.log(args)
			return Package.findById(args.id);
		}
	},

	Mutation: {
		createUser: (_, args) => {
			let newUser = User({
				name: args.name,
				role: args.role,
				email: args.email,
				username: args.username,
				password: bcrypt.hashSync(args.password, 10),
				contactNumber: args.contactNumber
			});

			return newUser.save();
		},

		createReservation: (_, args) => {
			let newReservation = Reservation({
				userId: args.userId,
				name: args.name,
				price: args.price,
				reservationDate: args.reservationDate,
				quantity: args.quantity,
				total: args.total,
				startTime: args.startTime,
				endTime: args.endTime,
				expertName: args.expertName,
				packages: args.packages
			});

			return newReservation.save();
		},

		createPackage: (_, args) => {
			let newPackage = Package({
				price: args.price,
				name: args.name
			});

			return newPackage.save();
		},

		createStaff: (_, args) => {
			let newStaff = Staff({
				name: args.name,
				expertise: args.expertise,
				status: args.status
			});

			return newStaff.save();
		},

		updateUser: (_, args) => {
			console.log(args);
			let condition = { _id: args.id };
			let update = {
				name: args.name,
				email: args.email,
				contactNumber: args.contactNumber
			};

			return User.findByIdAndUpdate(condition, update);
		},

		updateReservation: (_, args) => {
			console.log(args);
			let condition = { _id: args.id };
			let update = {
				userId: args.userId,
				name: args.name,
				price: args.price,
				reservationDate: args.reservationDate,
				quantity: args.quantity,
				total: args.total,
				startTime: args.startTime,
				endTime: args.endTime,
				packages: args.packages,
				expertName: args.expertName
			};

			return Reservation.findByIdAndUpdate(condition, update);
		},

		updateStaff: (_, args) => {
			console.log(args);
			let condition = { _id: args.id };
			let update = {
				name: args.name,
				expertise: args.expertise,
				status: args.status
			};

			return Staff.findByIdAndUpdate(condition, update);
		},

		updatePackage: (_, args) => {
			console.log(args);
			let condition = { _id: args.id };
			let update = {
				name: args.name,
				price: args.price
			};

			return Package.findByIdAndUpdate(condition, update);
		},

		deleteUser: (_, args) => {
			console.log(args.id);
			let condition = args.id;

			return User.findByIdAndDelete(condition).then((user, err) => {
				console.log(err);
				if (err || !user) {
					console.log("delete failed, no user found");
					return false;
				}

				console.log("user deleted");
				return true;
			});
		},

		deleteReservation: (_, args) => {
			let condition = args.id;

			return Reservation.findByIdAndDelete(condition).then(
				(user, err) => {
					if (err || !user) {
						console.log("delete failed, no reservations found");
						return false;
					}

					console.log("reservation deleted");
					return true;
				}
			);
		},

		deletePackage: (_, args) => {
			let condition = args.id;

			return Package.findByIdAndDelete(condition).then((user, err) => {
				if (err || !user) {
					console.log("delete failed, no packages found");
					return false;
				}

				console.log("packages deleted");
				return true;
			});
		},

		deleteStaff: (_, args) => {
			console.log(args);
			let condition = args.id;

			return Staff.findByIdAndDelete(condition).then((user, err) => {
				if (err || !user) {
					console.log("delete failed, no staff found");
					return false;
				}

				console.log("staff deleted");
				return true;
			});
		},

		logInUser: (_, args) => {
			console.log("trying to log in .....");
			console.log(args);

			return User.findOne({ username: args.username }).then(user => {
				if (user === null) {
					console.log("user not found");
					return null;
				}

				const hashedPassword = bcrypt.compareSync(
					args.password,
					user.password
				);
				if (!hashedPassword) {
					console.log("wrong password");
					return null;
				} else {
					console.log("login successfull");
					return user;
				}
			});
		}
	},

	// custom type object resolver

	UserType: {
		reservations: (parent, args) => {
			return Reservation.find({ userId: parent.id });
		}
	}
};

const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
