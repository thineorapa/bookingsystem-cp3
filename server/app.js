// backend server
// Declaring dependencies


// use/import the express library
const express = require('express');

// instantiate an express project
// serve our project using express
const app = express();

// use the mongoose library
const mongoose = require("mongoose");

let databaseURL = 	
	process.env.DATABASE_URL || 
	"mongodb+srv://dbSteen:Steen1234@nosqlsession-86ycp.mongodb.net/divine_spa?retryWrites=true&w=majority";

let cors = require("cors");

// database connection
mongoose.connect(databaseURL,{useNewUrlParser : true})

mongoose.connection.once("open", () => {
	console.log("now connected to the online MongoDB server")
})

app.use(cors()); 


// import the instantiation of the ApolloServer
const server = require("./queries/queries.js");

// the app will be served by the apollo server instead of express
server.applyMiddleware({
	app
	// path: "/batch43"
})

let port = process.env.PORT || 4001

// server initialization
app.listen(port, () => {
	console.log(`🚀  Server ready at http://localhost:4001 ${server.graphqlPath}`);
})
