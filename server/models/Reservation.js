const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reservationSchema = new Schema(
	{
		userId: {
			type: String,
			required: true
		},
		name: {
			type: String,
			required: true
		},
		reservationDate: {
			type: String,
			required: true
		},
		quantity: {
			type: Number,
			required: true
		},
		startTime: {
			type: String,
			required: true
		},
		endTime: {
			type: String,
			required: true
		},
		packages: {
			type: String,
			required: true
		},
		price: {
			type: Number,
			required: true
		},
		total: {
			type: Number,
			required: true
		},
		expertName: {
			type: String,
			required: true
		}
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("Reservation", reservationSchema);
