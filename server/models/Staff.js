const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const staffSchema = new Schema(
	{
		name: {
			type: String,
			required: true
		},
		expertise: {
			type: String,
			required: true
		},
		status: {
			type: Boolean,
			required: true
		}
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("Staff", staffSchema);
